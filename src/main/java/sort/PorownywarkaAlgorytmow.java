package sort;

import sort.strategies.BubbleSort;

import java.util.Random;

public class PorownywarkaAlgorytmow {
    // maksymalna wartość losowanych liczb
    public final static int PROG_WARTOSCI = 100;

    // strategia sortowania
    private AbstractSortStrategy strategia;
    // tablica do posortowania
    private Integer[] array;

    public PorownywarkaAlgorytmow() {
        losujLiczby(10000);
        setStrategia(new BubbleSort());
    }

    /**
     * Ustawiam strategie i przekazuję jej referencję do tablicy do posortowania.
     *
     * @param strategia - strategia do ustawienia
     */
    public void setStrategia(AbstractSortStrategy strategia) {
        this.strategia = strategia;
        if (this.strategia != null) {
            this.strategia.setArray(array.clone());
        }
    }

    /**
     * Losuje liczby z zakresu do PROG_WARTOSCI
     *
     * @param iloscLiczb - ilosc liczb do wylosowania
     */
    public void losujLiczby(int iloscLiczb) {
        array = new Integer[iloscLiczb];
        for (int i = 0; i < iloscLiczb; i++) {
            array[i] = new Random().nextInt(PROG_WARTOSCI);
        }
//        array = new Integer[]{0, 23, 15, 7, 12, 3, 4, 7};
    }

    /**
     * Wywoluje sortowanie.
     */
    public void sortuj() {
        if (strategia == null) {
            throw new IllegalStateException("Ustaw strategie sortowania");
        }
        strategia.sort();
    }

    /**
     * Wywoluje sortowanie z mierzeniem czasu.
     */
    public void sortAndTime() {
        if (strategia == null) {
            throw new IllegalStateException("Ustaw strategie sortowania");
        }
        strategia.sortAndMeasureTime();
    }

    /**
     * Wypisuje tablice w strategii.
     */
    public void print() {
        if (strategia == null) {
            throw new IllegalStateException("Ustaw strategie sortowania");
        }
        strategia.print();
    }
}
