package sort.strategies;

import sort.AbstractSortStrategy;

public class BubbleSort extends AbstractSortStrategy {

    public BubbleSort() {
        super();
    }

    public void sort() {
        // TODO: sortowanie bubble sort
        int n = array.length;

        int counter = 0;
        do {
            for (int i = 0; i < n - 1; i++) {
                counter++;
                if (array[i] > array[i + 1]) {
                    // swap
                    int tmp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = tmp;
                }
            }
            n--;
        } while (n > 1);
        System.out.println(counter);
    }
}
