package sort.strategies;

import sort.AbstractSortStrategy;

import java.util.Arrays;

public class MergeSort extends AbstractSortStrategy {
    int counter;

    public MergeSort() {
        super();
        counter = 0;
    }

    public void sort() {
        mergeSort(array, 0, array.length - 1);
        System.out.println(counter);
    }

    public void mergeSort(Integer[] tablica, int first, int last) {
        if (first != last) {
            int middle = ((last - first) / 2) + first;
            // split lewej
            mergeSort(tablica, first, middle);

//            System.out.println("first: " + first + ", middle: " + middle + ", last:" + last);
            // split prawej
            mergeSort(tablica, middle + 1, last);

            // laczenie (merge) obu stron
            merge(tablica, first, middle, last);
        }
    }

    public void merge(Integer[] tablica, int first, int middle, int last) {
        Integer[] tmp = new Integer[tablica.length];
        for (int i = 0; i < tablica.length; i++) {
            tmp[i] = tablica[i];
        }

        int i1 = first;
        int i2 = middle + 1;
        int indeks = first;

        // (i1 <= middle) - dopóki są elementy po lewej stronie
        // (i2 <= last) - dopóki są elementy po prawej stronie
        while ((i1 <= middle) && (i2 <= last)) {
            counter++;
            if (tmp[i1] > tmp[i2]) {
                tablica[indeks] = tmp[i2];
                i2++;
            } else {
                tablica[indeks] = tmp[i1];
                i1++;
            }
            indeks++;
        }

        for (int i = i1; i <= middle; i++) {
            tablica[indeks++] = tmp[i];
        }
        for (int i = i2; i <= last; i++) {
            tablica[indeks++] = tmp[i];
        }
    }

}
