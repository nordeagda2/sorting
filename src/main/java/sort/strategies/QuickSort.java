package sort.strategies;


import sort.AbstractSortStrategy;

public class QuickSort extends AbstractSortStrategy {

    int counter;

    public QuickSort() {
        super();
        counter = 0;
    }

    public void sort() {
        // TODO: sortowanie quick sort
        quickSort(array, 0, array.length - 1);
        System.out.println(counter);
    }

    public void quickSort(Integer[] tablica, int left, int right) {
        if (left < right) {
            int pivot = partition(tablica, left, right);
            quickSort(tablica, left, pivot - 1);
            quickSort(tablica, pivot + 1, right);
        }
    }

    public int partition(Integer[] tablica, int left, int right) {
        int x = tablica[left];
        int index_left = left - 1;
        int index_right = right + 1;
        while (true) {
            while (true) {
                index_left++;
                if (tablica[index_left] >= x) break;
            }
            while (true) {
                index_right--;
                if (tablica[index_right] <= x) break;
            }
            counter++;
            if (index_left < index_right) {
                int tmp = tablica[index_left];
                tablica[index_left] = tablica[index_right];
                tablica[index_right] = tmp;
            } else {
                return index_right;
            }
        }
    }
}
